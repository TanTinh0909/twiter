//dotenv
require('dotenv').config();

//connectDB
const { connectDB } = require('./configs/db');
connectDB();

const express = require('express');

const cors = require('cors');
//import route
const authRouter = require('./routers/authRouter');
const postRouter = require('./routers/postRoute');

//import error handler
const { errorHandler } = require('./middlewares/errorHandler');
const app = express();

//cors
app.use(cors());

//body parser
app.use(express.json());

//mount the router
app.use('/api/auth', authRouter);
app.use('/api/posts', postRouter);

//unhandler router 
app.all('*', (req, res, next) => {
    const err = new Error('The router can not be found');
    err.statusCode = 404;
    next(err);
})
app.use(errorHandler);
const port = process.env.APP_PORT;

app.listen(port, () => {
    console.log(`server is running on port ${port}`);
})