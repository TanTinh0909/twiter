const jwt = require('jsonwebtoken');

exports.checkCurrentUser = (req, res, next) => {
    //access authorzation from req header
    const Authorzation = req.header('Authorization');

    if (!Authorzation) {
        //error Authorization
        req.user = null;
        next();
    } else {
        const token = Authorzation.replace("Bearer ", "");
        //get token
        try {
            const { userId } = jwt.verify(token, process.env.APP_SECRET);
            //ASSIGN req
            req.user = { userId };
            next();
        } catch (error) {
            req.user = null;
            return res.status(403).json({ success: false, message: 'Invalid token' })
        }
    }
}