const jwt = require('jsonwebtoken');

exports.verifyToken = (req, res, next) => {
    //access authorzation from req header
    const Authorzation = req.header('Authorization');


    if (!Authorzation) {
        //error Authorization
        const err = new Error('Authorzation!!! is not correct');
        err.statusCode = 401;
        return next(err);
    }
    //get token
    try {
        const token = Authorzation.replace("Bearer ", "");
        const { userId } = jwt.verify(token, process.env.APP_SECRET);
        //ASSIGN req
        req.user = { userId };
        next();
    } catch (error) {
        return res.status(403).json({ success: false, message: 'Invalid token' })
    }

}